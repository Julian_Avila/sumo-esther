const int trigPin = 9;
const int echoPin = 10;
const int distanciaMinimaParaAvanzar = 30;
const int distanciaMinimaParaAtacar = 5;
const int valorLineaBlanca = 750;
boolean detectaLineaBlanca = false;

void setup() {
    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);
    Serial.begin(9600);
    delay(5000);
}

void loop() {
  detectaLineaBlanca = comprobarLineaBlanca();
  Serial.println(detectaLineaBlanca);
  int distanciaOponente = obtenerDistancia();
  Serial.println(distanciaOponente);

  if (distanciaOponente <= distanciaMinimaParaAvanzar && distanciaOponente > distanciaMinimaParaAtacar) {
    avanzar();
  } else if (distanciaOponente <= distanciaMinimaParaAtacar) {
    atacar();
  } else {
    buscarOponente();
  }
}


void atacar() {
  Serial.println("Atacando");

}

void avanzar() {
  Serial.println("Avanzando");
}

void buscarOponente() {
  Serial.println("Buscando");
}

bool comprobarLineaBlanca() {
  Serial.println(analogRead(A0));
  return analogRead(A0) >= valorLineaBlanca ? true : false;
}

int obtenerDistancia() {
  long duracion;
  int distanciaCM;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duracion = pulseIn(echoPin, HIGH);
  distanciaCM = duracion * 0.034 / 2;
  return distanciaCM;
}